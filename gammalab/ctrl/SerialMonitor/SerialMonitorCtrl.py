from PySide2.QtCore import QObject, QThreadPool, Signal

from serial import Serial
from serial.tools.list_ports import comports

from gammalab.ctrl.SerialMonitor.Message import Message, MessageType
from gammalab.ctrl.SerialMonitor.SerialPortSelector import SerialPortSelector
from gammalab.ctrl.Utils.Worker import Worker
from gammalab.ctrl.Utils.Queue import Queue


class SerialMonitorCtrl(QObject):
    serial_port: Serial = None
    send_queue = Queue()
    new_message = Signal(Message)
    __working_state__: bool = False
    __read_state__: bool = True
    _port_selector_: SerialPortSelector = None
    _port_name_: str = None

    def __init__(self, selector=None):
        super().__init__()
        self._port_selector_ = selector

    @staticmethod
    def get_ports_names():
        ports_list = comports()
        return ports_list

    def __init_thread__(self, process, progress_fn=None, on_complete=None):
        """Execute a monitor work in the background with a worker"""

        worker = Worker(fn=process)
        self.threadpool = QThreadPool()
        self.threadpool.start(worker)
        worker.signals.finished.connect(on_complete)
        worker.signals.progress.connect(progress_fn)
        return

    def start(self, read_timeout=1):
        try:
            self.__set_port__()
            self.serial_port = Serial(self._port_name_, timeout=read_timeout)
            self.new_message.emit(Message('Opening {0} ...'.format(str(self._port_name_)), MessageType.MONITOR_INFO))
            self.__working_state__ = True
            self.__init_thread__(self.__serial_work__, self.__serial_work_callback__, self.__close_serial__)
        except FileNotFoundError:
            self.new_message.emit(
                Message('Port name error. Program is unable to open serial port on {0}'.format(self._port_name_)),
                MessageType.MONITOR_INFO)
            self.stop()
        except Exception as e:
            self.new_message.emit(
                Message('Unknown exception: {}'.format(e), MessageType.MONITOR_INFO))
            if self.serial_port is not None:
                self.stop()

    def stop(self):
        """Stop serial monitor work"""
        self.serial_port.close()
        self.__working_state__ = False

    def __close_serial__(self):
        self.new_message.emit(Message('Serial port closed', MessageType.MONITOR_INFO))
        del self.serial_port

    def __serial_work_callback__(self, msg: str):
        """Update progress"""
        self.new_message.emit(Message(msg))
        return

    def __serial_work__(self, progress_callback):
        while self.serial_port.is_open:
            if self.serial_port.readable():
                data = self.__read_data__()
                if data != b'' and data != b'\r\n' and data != b'\n':
                    msg_in = self.decode(data)
                    if self.__read_state__:
                        progress_callback.emit(msg_in)
            if not self.send_queue.is_empty():
                msg_out = self.send_queue.get().encode() + b'\r\n'
                self.serial_port.write(msg_out)

    def __read_data__(self):
        try:
            data = self.serial_port.readline()
        except UnicodeDecodeError as e:
            self.new_message.emit(Message('Unicode Error... Serial stopping'))
            self.__working_state__ = False
            self.__close_serial__()
        except TypeError:
            data = b''
        return data

    def decode(self, data):
        try:
            msg = str(data.decode()).rstrip('\r\n')
        except UnicodeDecodeError:
            return ' '
        except Exception:
            return ' '
        else:
            return msg

    def send(self, msg: str):
        ''' Write message to serial port '''
        self.send_queue.put(msg)
        self.new_message.emit(Message(msg, MessageType.OUT))

    def set_selector(self, selector: SerialPortSelector) -> None:
        self._port_name_ = None
        self._port_selector_ = selector

    def del_selector(self) -> None:
        self._port_selector_ = None

    def port_name(self, name: str, force: bool = 0) -> None:
        ''' It give possibility  '''
        if self._port_selector_ is None or force:
            self._port_name_ = name
            if self._port_selector_ is not None:
                self._port_selector_ = None

    def __set_port__(self):
        if self._port_selector_ is not None:
            self._port_name_ = self._port_selector_.port
        elif self._port_name_ is None:
            raise AttributeError(
                'Due to the lack of a selector, it is necessary to set the port name first. Use port_name ().')

    @property
    def read_state(self):
        return self.__read_state__

    @read_state.setter
    def read_state(self, state: bool):
        self.__read_state__ = state
