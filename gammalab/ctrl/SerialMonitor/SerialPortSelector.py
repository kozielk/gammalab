from serial.tools.list_ports import comports


class SerialPortSelector:
    _name: str = None

    @property
    def port(self) -> str: return self._name

    @port.setter
    def port(self, port_name: str) -> None:
        if self.__valid_name__(port_name):
            self._name = port_name
        else:
            self._name = None

    def ports(self) -> [str]:
        return SerialPortSelector.available_ports()

    @staticmethod
    def available_ports() -> [str]:
        port_list = []
        for port in comports():
            port_list.append(port.device)
        return port_list

    def __valid_name__(self, name: str) -> None:
        if name in SerialPortSelector.available_ports():
            return True
        return False

