from PySide2.QtCore import Signal
from PySide2.QtGui import QMouseEvent
from PySide2.QtWidgets import QComboBox, QStyle, QStyleOptionComboBox

from gammalab.ctrl.SerialMonitor.SerialPortSelector import SerialPortSelector


class QComboBoxPortSelector(SerialPortSelector, QComboBox):
    arrowClicked = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__fill__()
        self.arrowClicked.connect(self.__fill__)
        self.currentTextChanged.connect(self.__set_port__)

    @property
    def port(self) -> str:
        return self._name

    def mousePressEvent(self, e: QMouseEvent):
        super().mousePressEvent(e)

        opt = QStyleOptionComboBox()
        opt.initFrom(self)
        opt.subControls = QStyle.SC_All
        opt.activeSubControls = QStyle.SC_None
        opt.editable = self.isEditable()
        cc = self.style().hitTestComplexControl(
            QStyle.CC_ComboBox, opt, e.pos(), self)
        if cc == QStyle.SC_ComboBoxArrow:
            self.arrowClicked.emit()

    def __fill__(self):
        self.clear()
        ports_list = self.ports()
        for port in ports_list:
            self.addItem(port)

        self.update()

    def __set_port__(self):
        if self.__valid_name__(self.currentText()):
            self._name = self.currentText()
        else:
            self._name = None
