from enum import Enum


class MessageType(Enum):
    """ Enum type class, describes direction the message related to serial monitor.
    IN is message from serial port to program
    OUT is message from program to serial port"""
    IN = 0
    OUT = 1
    MONITOR_INFO = 2


class Message:
    body: str
    direction: MessageType

    def __init__(self, body: str, direction: MessageType = MessageType.IN):
        self.body = body
        self.direction = direction

    def toByte(self):
        return self.body.encode()

    def __str__(self):
        return self.body

    def __repr__(self):
        return self.body
