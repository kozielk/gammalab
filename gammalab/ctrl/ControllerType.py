from enum import Enum


class ControllerType(Enum):
    Arduino = 0
    ArduinoMega = 1
    RaspberryPi = 2

    @classmethod
    def from_name(cls, name):
        for ctrl, ctrl_name in ControllerType.items():
            if ctrl_name == name:
                return ctrl
        raise ValueError('{} is not a valid station name'.format(name))

    def to_name(self) -> str:
        return ControllerType[self.value]
