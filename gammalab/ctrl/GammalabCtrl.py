import numpy as np
from PySide2.QtCore import QObject, Signal, QTimer, SIGNAL

from gammalab.ctrl.ControllerArduino import ControllerArduino


class GammalabCtrl(QObject):
    probeFinish = Signal()
    order_tab = []
    empulses = []
    mode: str = 'calibrate'
    calibrateState: bool = False

    def __init__(self, port_name, parent=None):
        super().__init__(parent)
        self.device_ctrl = ControllerArduino(port_name)
        self.device_ctrl.devCalibrate.connect(self.changeCalibrate)
        self._image_: np.array

    def changeCalibrate(self):
        self.calibrateState = True

    def calibrate(self):
        if not self.calibrateState:
            self.device_ctrl.headReceive.connect(lambda: self.device_ctrl.sendOrder.emit('calibrate'))
        else:
            self.order_tab.clear()
            self.order_tab.append('calibrate')
            self.device_ctrl.sendOrder.emit(self.order_tab.pop(0))


    def read(self, mode: str = 'Free', **kwargs):
        if not self.calibrateState:
            return
        self.order_tab.clear()
        self.patient = kwargs.get('patient', '0')
        self.order_tab.append('load_patient {}'.format(self.patient))
        self.order_tab.append('start_patient')
        self.mode = mode
        if self.mode == 'Free':
            self.make_orders()
        elif self.mode == 'Number':
            self.num = kwargs.get('number', 100)
            self.make_orders(self.num)
        elif self.mode == 'Time':
            self.make_orders()
            QTimer.singleShot(kwargs.get('time', 12)*1000, self.stop)
        self.device_ctrl.orderComplete.connect(self.execute_orders)
        self.device_ctrl.orderComplete.emit()

    # def load_patient(self, img_path):

    def stop(self):
        self.order_tab.clear()
        self.device_ctrl.sendOrder.emit('stop')
        self.empulses = self.device_ctrl.get_frames()
        try:
            self.device_ctrl.orderComplete.disconnect()
        except RuntimeError:
            pass
        self.probeFinish.emit()

    def execute_orders(self):
        if self.calibrateState:
            if len(self.order_tab) == 0:
                if self.mode == 'Number':
                    if self.device_ctrl.get_frames().size() == self.num:
                        self.stop()
                        self.make_orders()
                if self.mode == 'Time' or self.mode == 'Free':
                    if self.device_ctrl.get_frames().size() % 20 == 18:
                        self.make_orders()
            else:
                self.device_ctrl.sendOrder.emit(self.order_tab.pop(0))

    def make_orders(self, amount=20) -> None:
        self.order_tab.append('read_matrix {}'.format(amount))

    def image_arr(self, x: int = 16, y: int = 16):
        img = np.ones((x, y), np.uint8)
        img_pixel_width = float(self.empulses.img_width() / x)
        img_pixel_height = float(self.empulses.img_height() / y)
        for emp in self.empulses.elements2absolute():
            if emp.energy > 100:
                x_cor = int(emp.x / img_pixel_width)
                y_cor = int(emp.y / img_pixel_height)
                if 0 <= x_cor < x and 0 <= y_cor < y:
                    img[x_cor][y_cor] = img[x_cor][y_cor] + 10
                    if img[x_cor][y_cor] > 255:
                        img[x_cor][y_cor] = 255

        return img
