from gammalab.ctrl.ControllerType import ControllerType
from gammalab.ctrl.Order import Order


class ControllerBase:
    __ctrl_type__: ControllerType

    def execute_order(self, order: Order):
        pass

    def read_sensor(self):
        pass

    def read_matrix(self):
        pass

    def set_digital(self):
        pass

    def __str__(self) -> str:
        return 'Gammalab controller ' + self.__ctrl_type__.to_name()