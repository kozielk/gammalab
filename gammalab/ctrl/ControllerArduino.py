from PySide2.QtCore import QObject
from PySide2.QtCore import Signal

from gammalab.ctrl.ControllerBase import ControllerBase
from gammalab.ctrl.ControllerType import ControllerType
from gammalab.ctrl.Gammalab import GamlSeries
from gammalab.ctrl.Gammalab.GamlFrame import GamlFrame
from gammalab.ctrl.SerialMonitor.Message import Message, MessageType
from gammalab.ctrl.SerialMonitor.SerialMonitorCtrl import SerialMonitorCtrl

import numpy as np
import json

corr_mat = np.array([[0, 0, 0, 1, 1, 1, 1, 3],
                     [0, 1, 1, 2, 0, 2, 3, 5],
                     [0, 1, 1, 3, 1, 3, 2, 5],
                     [1, 3, 3, 5, 3, 5, 5, 6],
                     [0, 1, 1, 3, 1, 3, 3, 4],
                     [1, 3, 3, 6, 3, 5, 5, 7],
                     [1, 3, 3, 5, 3, 5, 5, 7],
                     [4, 5, 5, 7, 5, 7, 7, 9]])


class ControllerArduino(ControllerBase, QObject):
    __orders__ = ('read_matrix',
                  'set_col_s',
                  'set_row_s',
                  'change_size',
                  'p_matrix',
                  'change_apin')
    dev_params: dict
    __ctrl_type__: ControllerType = ControllerType.Arduino
    monitor: SerialMonitorCtrl = None
    frames = GamlSeries()
    __msg_queue = []
    __order_head__: str = None
    __monitor_head__: str = None
    mode: str = 'time'
    orderComplete = Signal()
    sendOrder = Signal(str)
    devCalibrate = Signal()
    devParamsChanged = Signal()
    headReceive = Signal()

    def __init__(self, port_name: str):
        super().__init__()
        self.monitor = SerialMonitorCtrl()
        self.monitor.port_name(port_name)
        self.monitor.new_message.connect(self.__get_message__)
        self.monitor.start()
        self.sendOrder.connect(self.send_order)

    def send_order(self, order):
        self._set_order_head_(self._get_ord_head_(order))
        self.monitor.send(order)

    def _get_ord_head_(self, order: str):
        order_tab = order.split(' ')
        order_head = order_tab[0]
        return order_head

    def _set_order_head_(self, head):
        self.__order_head__ = '#' + head + '#'

    def __get_message__(self, msg: Message):
        msg_text = str(msg)
        print(msg_text)
        if msg.direction == MessageType.IN:
            self.__msg_queue.append(msg_text)
            if msg_text == '<-######################->':
                self.get_dev_head()
                self.headReceive.emit()
            if msg_text == '<-##calibrate##->':
                self.devCalibrate.emit()
            if msg_text == '#||#':
                self.__get_matrix__()
                self.orderComplete.emit()

    def get_dev_head(self):
        idx = self.__msg_queue.index('<-######################->')
        data = self.__msg_queue[idx - 1]
        self.dev_params = json.loads(data)
        self.frames = GamlSeries(**self.dev_params)
        self.devParamsChanged.emit()

    def __get_matrix__(self):
        __process_data__ = []
        if '#read_matrix#' in self.__msg_queue:
            idx = self.__msg_queue.index('#read_matrix#')
            idx = idx + 1
            while self.__msg_queue[idx] != '#||#':
                __process_data__.append(self.__msg_queue[idx])
                idx = idx + 1
            self.__msg_queue = self.__msg_queue[idx:]
            self._get_empulse_(__process_data__)

    def _map_row_(self, row_data: str):
        val_list = row_data.split('|')[:-1]
        arr = np.array(val_list)
        arr = np.reshape(arr, (8, 8))
        return arr.astype(np.int)

    def _get_empulse_(self, data):
        idx = 0
        row_arr = np.zeros((8, 8), int)
        for row in data:
            row_arr = self._map_row_(row)
            idx += 1
        # print(GamlFrame.from_data(row_arr, corr_mat).get_empulse())
        self.frames.add(GamlFrame.from_data(row_arr, corr_mat).get_empulse())

    def get_frames(self) -> GamlSeries:
        return self.frames

    def __repr__(self) -> str:
        return super().__repr__()
