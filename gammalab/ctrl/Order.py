from gammalab.ctrl.OrderType import OrderType

class Order:
    __order_type: OrderType = None
    __pin_target = None
    __state: bool = None

    def __init__(self, **kwargs):
        self.state = kwargs.get('state', None)
        self.pin = kwargs.get('pin', None)
        self.order_type = kwargs.get('type', None)

    def update(self):
        return {'type': self.__order_type,
                'pin': self.__pin_target,
                'state': self.__state}

    def execute(self):
        self.update()

    def valid(self):
        if self.order_type is OrderType.READ_A or self.order_type is OrderType.SET_D:
            if self.pin[0] != 'A' or self.pin[0] != 'a':
                return False
            return True
        elif self.order_type is OrderType.READ_A or self.order_type is OrderType.SET_A:
            if self.pin[0] != 'D' or self.pin[0] != 'd':
                return False
            return True

    def to_byte(self):
        str_repr = self.order_type.to_name() + ' ' + self.pin[1:]
        if self.state is True:
            str_repr = str_repr + ' 1'
        return str_repr.encode() + b'\r'


    @property
    def pin(self): return self.__pin_target

    @pin.setter
    def pin(self, pin):
        self.__pin_target = pin

    @property
    def order_type(self) -> OrderType: return self.__order_type

    @order_type.setter
    def order_type(self, o_type: OrderType):
        self.__order_type = o_type

    @property
    def state(self) -> bool: return self.__state

    @state.setter
    def state(self, state: bool):
        self.__state = state

    def __str__(self):
        str_repr = self.order_type.to_name() + ' ' + self.pin[1:]
        if self.state is True:
            str_repr = str_repr + ' 1'
        return str_repr

    def __repr__(self):
        str_repr = self.order_type.to_name() + ' ' + self.pin[1:]
        if self.state is True:
            str_repr = str_repr + ' 1'
        return str_repr

if __name__ == '__main__':
    o = Order()
    o.state = True
    o.order_type = OrderType.SET_D
    o.pin = 'D33'


