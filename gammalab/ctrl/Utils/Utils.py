import PIL
from PIL import Image
from numpy import asarray


def arrFromImg(img_path):
    image = Image.open(img_path).convert('L')
    image.save('graysclae.png')
    image = image.resize((16, 8), PIL.Image.BILINEAR )
    data = asarray(image)

    return data
