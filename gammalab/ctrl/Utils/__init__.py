def get_style(path: str):
    """ Return CSS file content in string format"""

    css_file = open(path, 'r')
    style = css_file.read()

    return style