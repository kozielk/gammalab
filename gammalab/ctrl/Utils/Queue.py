class Node:

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)

class Queue:

    def __init__(self):
        self.head = None
        self.tail = None

    def is_empty(self):
        return not self.head

    def put(self, data):            # constat-time
        node = Node(data)   # next jest domyslnie None
        if self.is_empty():
            self.head = self.tail = node
        else:                   # dajemy na koniec listy
            self.tail.next = node
            self.tail = node

    def get(self):                  # constat-time
        data = self.head.data
        self.head = self.head.next
        if self.is_empty():       # zabezpieczenie
            self.tail = None
        return data