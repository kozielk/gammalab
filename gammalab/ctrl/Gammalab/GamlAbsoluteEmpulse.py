from gammalab.ctrl.Gammalab import GamlEmpulse


class GamlAbsoluteEmpulse(GamlEmpulse):
    @property
    def x(self):
        return super().x

    @property
    def y(self):
        return super().y

    @property
    def energy(self):
        return super().energy

    _x: float
    _y: float
    _energy: int

    def __init__(self, pos: tuple, energy: int):
        super().__init__(pos, energy)

    def __repr__(self):
        return 'Absolute Empulse of Energy: {0} at position ({1}, {2})'.format(self.energy, self.x, self.y)

    def __str__(self):
        return 'Absolute Empulse of Energy: {0} at position ({1}, {2})'.format(self.energy, self.x, self.y)