import numpy as np

from gammalab.ctrl.Gammalab.GamlEmpulse import GamlEmpulse


class GamlFrame(object):
    _size: list
    _weight: float
    _height: float
    _rows: int
    _cols: int
    _matrix: np.array

    def __init__(self, **kwargs) -> None:
        self._rows = kwargs.pop('rows', 8)
        self._cols = kwargs.pop('cols', 8)

    def frame(self, matrix: np.array, corr_mat: np.array):
        self._matrix = matrix - corr_mat
        self._rows = matrix.shape[0]
        self._cols = matrix.shape[1]

    def get_empulse(self):
        enrg = 0
        x_pos = 0
        y_pos = 0
        it = np.nditer(self._matrix, flags=['multi_index'], op_flags=['writeonly'])
        maxi = np.max(self._matrix)
        pmax = maxi * 0.3
        for x in it:
            if x >= pmax:
                x_pos = x_pos + (x * it.multi_index[0])
                y_pos = y_pos + (x * it.multi_index[1])
                enrg = enrg + x

        if enrg <= 0:
            enrg = 1

        return GamlEmpulse((x_pos/enrg, y_pos/enrg), enrg)

    @staticmethod
    def from_data(data: np.array, corr_mat: np.array):
        frame = GamlFrame()
        frame.frame(data, corr_mat)
        # print(frame._matrix)
        return frame

    @property
    def rows(self) -> int:
        return self._rows

    @rows.setter
    def rows(self, value: int):
        self._rows = value
        self.__clear__()

    @property
    def cols(self) -> int:
        return self._cols

    @cols.setter
    def cols(self, value: int):
        self._cols = value
        self.__clear__()

    def __clear__(self):
        self._size = [self.cols, self.rows]
        self._matrix = np.zeros(self.rows, self.cols)

    def __str__(self) -> str:
        return 'GammaLab Frame Matrix: {} x {}'.format(self.rows, self.cols)

    def __repr__(self) -> str:
        return 'GammaLab Frame Matrix: {} x {}'.format(self.rows, self.cols)
