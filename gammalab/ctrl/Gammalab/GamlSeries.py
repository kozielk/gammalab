import numpy as np

from gammalab.ctrl.Gammalab.GamlAbsoluteEmpulse import GamlAbsoluteEmpulse
from gammalab.ctrl.Gammalab.GamlEmpulse import GamlEmpulse


class GamlSeries(object):
    _x_rows: int = 8
    _y_cols: int = 8
    _x_offset: float = 1
    _y_offset: float = 1
    _series: list

    def __init__(self, **kwargs) -> None:
        super().__init__()
        self._x_rows = kwargs.get('rows', 8)
        self._y_cols = kwargs.get('cols', 8)
        self._x_offset = kwargs.get('x_offset', 8)
        self._y_offset = kwargs.get('y_offset', 8)
        self._series = []

    def add(self, impulse: GamlEmpulse):
        self._series.append(impulse)

    def get_series(self):
        return self._series

    def get_image(self, index: int = 0):
        return self._series[index]

    def size(self):
        return len(self._series)

    def clear(self):
        self._series = []

    @property
    def m_rows(self):
        return self._x_rows

    @property
    def m_cols(self):
        return self._y_cols

    def img_width(self): return float((self._x_rows - 1) * self._x_offset)
    def img_height(self): return float((self._y_cols - 1) * self._y_offset)

    def m_size(self, m_size: tuple):
        self._x_rows = m_size[0]
        self._y_rows = m_size[1]

    def m_offset(self, m_offset: tuple):
        self._x_offset = m_offset[0]
        self._y_offset = m_offset[1]

    def elements2absolute(self) -> list:
        a_series = []
        for el in self._series:
            a_el = GamlAbsoluteEmpulse((el.x * self._x_offset, el.y * self._y_offset), el.energy)
            a_series.append(a_el)

        return a_series
