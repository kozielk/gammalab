class GamlEmpulse(object):
    _x: int
    _y: int
    _energy: int

    def __init__(self, pos: tuple, energy: int):
        self._x = pos[0]
        self._y = pos[1]
        self._energy = energy

    @property
    def x(self): return self._x

    @property
    def y(self): return self._y

    @property
    def energy(self): return self._energy

    def __repr__(self):
        return 'Empulse of Energy: {0} at position ({1}, {2})'.format(self.energy, self.x, self.y)

    def __str__(self):
        return 'Empulse of Energy: {0} at position ({1}, {2})'.format(self.energy, self.x, self.y)