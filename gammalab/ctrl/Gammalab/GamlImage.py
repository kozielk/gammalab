from typing import List
import numpy as np


class GamlImage(object):
    _size: List[int]
    _rows: int
    _cols: int
    _matrix: np.array

    def __init__(self, **kwargs) -> None:
        self._rows = kwargs.pop('rows', 8)
        self._cols = kwargs.pop('cols', 8)

    @property
    def rows(self) -> int:
        return self._rows

    @rows.setter
    def rows(self, value: int):
        self._rows = value
        self.__clear__()

    @property
    def cols(self) -> int:
        return self._cols

    @cols.setter
    def cols(self, value: int):
        self._cols = value
        self.__clear__()

    def __clear__(self):
        self._size = [self.cols, self.rows]
        self._matrix = np.zeros(self.rows, self.cols)


    def __str__(self) -> str:
        return 'GammaLab Image Matrix: {} x {}'.format(self.rows, self.cols)

    def __repr__(self) -> str:
        return 'GammaLab Image Matrix: {} x {}'.format(self.rows, self.cols)


