from PySide2.QtCore import QObject
from PySide2.QtGui import Qt
from PySide2.QtWidgets import QWidget, QSpinBox, QDoubleSpinBox, QVBoxLayout, QHBoxLayout, QLabel



class GamlParamsWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()

    def init_ui(self):
        self.img_x = QSpinBox()
        self.img_x.setValue(16)
        self.img_x.setSingleStep(1)
        self.img_x.setMinimum(2)
        self.img_x.setMaximum(64)

        self.img_y = QSpinBox()
        self.img_y.setValue(16)
        self.img_y.setSingleStep(1)
        self.img_y.setMinimum(2)
        self.img_y.setMaximum(64)

        self.time = QSpinBox()
        self.time.setValue(10)
        self.time.setSingleStep(10)
        self.time.setMinimum(10)

        self.number = QSpinBox()
        self.number.setValue(10)
        self.number.setSingleStep(10)
        self.number.setMinimum(10)
        self.number.setMaximum(10000)

        self.rows_label = QLabel('Rows: ...')
        self.cols_label = QLabel('Cols: ...')
        self.x_off_label = QLabel('X Offset: ...')
        self.y_off_label = QLabel('Y Offset: ...')

        layout1 = QVBoxLayout()
        layout1.setAlignment(Qt.AlignTop)
        layout1.setAlignment(Qt.AlignLeft)
        layout1.addLayout(self._add_label_(self.img_x, 'Image width (pixels):  '))
        layout1.addLayout(self._add_label_(self.img_y, 'Image height (pixels): '))

        layout = QVBoxLayout()
        layout.setAlignment(Qt.AlignTop)
        layout.setAlignment(Qt.AlignLeft)
        layout.addWidget(self.rows_label)
        layout.addWidget(self.cols_label)
        layout.addWidget(self.x_off_label)
        layout.addWidget(self.y_off_label)

        layout2 = QVBoxLayout()
        layout2.setAlignment(Qt.AlignTop)
        layout2.setAlignment(Qt.AlignLeft)
        layout2.addLayout(self._add_label_(self.time, 'Time (sec):  '))
        layout2.addLayout(self._add_label_(self.number, 'Number:  '))

        mainLayout = QHBoxLayout(self)
        mainLayout.setAlignment(Qt.AlignLeft)
        mainLayout.addLayout(layout)
        mainLayout.addLayout(layout2)
        mainLayout.addLayout(layout1)

    def _add_label_(self, widget, text: str):
        layout = QHBoxLayout()
        layout.addWidget(QLabel(text))
        layout.addWidget(widget)

        return layout

    def updateDeviceParams(self, **kwargs):
        self.rows_label.setText('Rows: {}'.format(kwargs.get('rows', '...')))
        self.cols_label.setText('Cols: {}'.format(kwargs.get('cols', '...')))
        self.x_off_label.setText('X Offset: {}'.format(kwargs.get('x_offset', '...')))
        self.y_off_label.setText('Y Offset: {}'.format(kwargs.get('x_offset', 'Y Offset: ...')))

    def getParams(self):
        return {'rows': self.img_x.value(), 'cols': self.img_y.value(), 'time': self.time.value(), 'number': self.number.value()}

    def changeMode(self, mode: str):
        if mode == 'Time':
            self.time.setEnabled(True)
            self.number.setEnabled(False)
        elif mode == 'Number':
            self.time.setEnabled(False)
            self.number.setEnabled(True)
        else:
            self.time.setEnabled(False)
            self.number.setEnabled(False)
