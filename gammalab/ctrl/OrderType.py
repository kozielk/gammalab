from enum import Enum


class OrderType(Enum):
    INIT_PIN = 0
    SET_D = 1
    READ_D = 2
    SET_A = 3
    READ_A = 4
    STOP = 5
    READ_MATRIX = 6

    @classmethod
    def from_name(cls, name):
        for order, order_name in OrderType.items():
            if order_name == name:
                return order
        raise ValueError('{} is not a valid station name'.format(name))

    def to_name(self) -> str:
        return self.name

