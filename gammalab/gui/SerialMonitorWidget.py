from PySide2.QtWidgets import QWidget, QTextEdit, QLineEdit, QVBoxLayout, QPushButton, QHBoxLayout

from gammalab.ctrl.SerialMonitor.Message import Message, MessageType
from gammalab.ctrl.SerialMonitor.QComboBoxPortSelector import QComboBoxPortSelector
from gammalab.ctrl.SerialMonitor.SerialMonitorCtrl import SerialMonitorCtrl

class SerialMonitorWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()

    def init_ui(self):
        self.ports_box = QComboBoxPortSelector()

        self.monitor_input = QLineEdit()
        self.monitor_output = QTextEdit()
        self.monitor_output.setReadOnly(True)
        self.submit_btn = QPushButton('Send')

        self.start_btn = QPushButton('Start Reading')
        self.stop_btn = QPushButton('Stop Reading')

        left_layout = QVBoxLayout()
        left_layout.addWidget(self.start_btn)
        left_layout.addWidget(self.stop_btn)

        input_layout = QHBoxLayout()
        input_layout.addWidget(self.monitor_input)
        input_layout.addWidget(self.submit_btn)

        monitor_layout = QVBoxLayout()
        monitor_layout.addWidget(self.ports_box)
        monitor_layout.addLayout(input_layout)
        monitor_layout.addWidget(self.monitor_output)

        layout = QHBoxLayout(self)
        layout.addLayout(left_layout)
        layout.addLayout(monitor_layout)

        self.submit_btn.clicked.connect(self.submit_btn_click)
        self.start_btn.clicked.connect(self.start_btn_click)

    def print_message(self, message: Message, pre: str = '', after: str = ''):
        ''' Monitor messages output. Related to message type is different pre messages signs.
            For messages from user : ">>"
            For monitor info: "(MI)" '''

        msg = message.body
        if message.direction is MessageType.OUT:
            pre = pre + '>> '
        elif message.direction is MessageType.MONITOR_INFO:
            pre = pre + '(MI) '
        self.monitor_output.append(pre + msg + after)

    def submit_btn_click(self, ctrl: SerialMonitorCtrl):
        ctrl.send(self.monitor_input.text())
        self.monitor_input.clear()

    def stop_btn_click(self, ctrl: SerialMonitorCtrl = None):
        try:
            ctrl.stop()
            del ctrl
        finally:
            self.ports_box.setDisabled(False)
            self.start_btn.setDisabled(False)
            self.stop_btn.setDisabled(True)
            self.stop_btn.clicked.disconnect()
            self.submit_btn.clicked.disconnect()

    def start_btn_click(self):
        if self.ports_box.port is not None:
            self.start_btn.setDisabled(True)
            self.ports_box.setDisabled(True)
            self.monitor_output.clear()

            ctrl = SerialMonitorCtrl(self.ports_box)
            ctrl.new_message.connect(self.print_message)

            self.submit_btn.clicked.connect(lambda: self.submit_btn_click(ctrl))
            self.stop_btn.setDisabled(False)
            self.stop_btn.clicked.connect(lambda: self.stop_btn_click(ctrl))

            ctrl.start()
        else: return

    def __switch_buttons__(self):
        self.start_btn.setDisabled(True)
