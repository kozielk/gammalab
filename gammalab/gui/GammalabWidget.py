import numpy as np
from PIL import Image
from PySide2.QtCore import Qt
from PySide2.QtGui import QPixmap, QImage
from PySide2.QtWidgets import QWidget, QLabel, QHBoxLayout, QPushButton, QVBoxLayout, QMessageBox, QRadioButton, \
    QButtonGroup, QCheckBox, QFileDialog

from gammalab.ctrl.Gammalab import GamlSeries
from gammalab.ctrl.Gammalab.GamlParams import GamlParamsWidget
from gammalab.ctrl.GammalabCtrl import GammalabCtrl
from gammalab.ctrl.SerialMonitor.QComboBoxPortSelector import QComboBoxPortSelector
from gammalab.ctrl.Utils.Utils import arrFromImg
from gammalab.gui.ui_elements.GamlImageWdg import GamlImageWdg


class GammalabWidget(QWidget):
    ctrl: GammalabCtrl = None
    data: GamlSeries
    value_label: QLabel
    save_path: str = ''
    mode: str = 'Free'
    patientImgPath: str = 'gammalab/resources/blank.png'
    patientImgLinear: str
    img = np.zeros((8, 8), np.uint8)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()

    def init_ui(self):
        self.ports_box = QComboBoxPortSelector()
        self.start_btn = QPushButton('Start')
        self.start_btn.setDisabled(True)
        self.stop_btn = QPushButton('Stop')
        self.stop_btn.setDisabled(True)
        self.calibrate_btn = QPushButton('Calibrate')
        self.loadPatientBtn = QPushButton('Load Patient Image')
        self.loadPatientBtn.clicked.connect(self.loadPatientBtnClick)

        self.value_label = QLabel('Frames: 0')
        self.parmsWdg = GamlParamsWidget()
        self.parmsWdg.changeMode(mode=self.mode)
        self.patient_lbl = QLabel()
        self.patient_image = QImage(self.patientImgPath)
        self.patient_pixmap = QPixmap(self.patient_image)
        self.patient_lbl.setPixmap(self.patient_pixmap.scaled(self.patient_lbl.size(), Qt.KeepAspectRatio))

        self.gaml_lbl = QLabel()
        self.gaml_img = QImage(r'gammalab/resources/blank_g.png')
        self.gaml_pixmap = QPixmap(self.gaml_img)
        self.gaml_lbl.setPixmap(self.gaml_pixmap.scaled(self.gaml_lbl.size(), Qt.KeepAspectRatio))

        self.image_wdg = GamlImageWdg()

        self.freeBtn = QRadioButton('Free')
        self.freeBtn.setChecked(True)
        self.numberBtn = QRadioButton('Number')
        self.timeBtn = QRadioButton('Time')

        self.askSaveCheck = QCheckBox('Ask everytime')
        self.askSaveCheck.stateChanged.connect(self.askCheckChange)
        self.selectPathBtn = QPushButton('Select Directory')
        self.selectPathBtn.clicked.connect(self.selectPathBtnClick)
        self.saveLabel = QLabel('Image file directory: ' + self.save_path)

        self.modeButtons = QButtonGroup()
        self.modeButtons.addButton(self.freeBtn)
        self.modeButtons.addButton(self.numberBtn)
        self.modeButtons.addButton(self.timeBtn)
        self.modeButtons.buttonClicked.connect(self.changeMode)

        btnLayout = QVBoxLayout()
        btnLayout.addWidget(self.start_btn)
        btnLayout.addWidget(self.stop_btn)

        f_layout = QVBoxLayout()
        f_layout.addWidget(self.ports_box)
        f_layout.addWidget(self.value_label)

        modeBtnLayout = QVBoxLayout()
        modeBtnLayout.addWidget(self.freeBtn)
        modeBtnLayout.addWidget(self.numberBtn)
        modeBtnLayout.addWidget(self.timeBtn)

        head_layout = QHBoxLayout()
        head_layout.setAlignment(Qt.AlignLeft)
        head_layout.addLayout(btnLayout)
        head_layout.addLayout(f_layout)
        head_layout.addLayout(modeBtnLayout)
        head_layout.addWidget(self.calibrate_btn)
        head_layout.addWidget(self.loadPatientBtn)
        saveLayout = QHBoxLayout()
        saveLayout.addWidget(self.saveLabel)
        saveLayout.addWidget(self.askSaveCheck)
        saveLayout.addWidget(self.selectPathBtn)

        gamlImgLayout = QVBoxLayout()
        gamlImgLayout.addWidget(QLabel('Gammalab Image'))
        gamlImgLayout.addWidget(self.gaml_lbl)
        patientImgLayout = QVBoxLayout()
        patientImgLayout.setAlignment(Qt.AlignTop)
        patientImgLayout.addWidget(QLabel('Patient Image'))
        patientImgLayout.addWidget(self.patient_lbl)

        imagesLayout = QHBoxLayout()
        imagesLayout.setAlignment(Qt.AlignCenter)
        imagesLayout.setAlignment(Qt.AlignTop)
        imagesLayout.addLayout(gamlImgLayout)
        imagesLayout.addLayout(patientImgLayout)

        layout = QVBoxLayout(self)

        # layout.setAlignment(Qt.AlignCenter)
        layout.addLayout(head_layout)
        layout.addWidget(self.parmsWdg)
        layout.addLayout(saveLayout)
        layout.addSpacing(30)
        layout.addLayout(imagesLayout)

        self.start_btn.clicked.connect(self.start_btn_click)
        self.calibrate_btn.clicked.connect(self.calibrate_btn_click)
        self.patientLinFromImg()

    def start_btn_click(self):
        if self.ports_box.port is not None:
            if self.ctrl is None:
                self._init_ctrl_()
            self.ctrl.read(mode=self.mode, patient=self.patientLinFromImg(), **self.parmsWdg.getParams())
            self.start_btn.setText('Working...')
            self.start_btn.setDisabled(True)
            self.calibrate_btn.setDisabled(True)
            self.stop_btn.setDisabled(False)
            self.ctrl.device_ctrl.orderComplete.connect(self.updateFramesCounter)

    def calibrate_btn_click(self):
        if self.ports_box.port is not None:
            if self.ctrl is None:
                self._init_ctrl_()
            self.ctrl.calibrate()
            self.calibrate_btn.setDisabled(True)
        else:
            QMessageBox.information(self, 'Approve port',
                                    'Check port name selector. It possible you have to only approve it.')

    def _probe_finish_(self):
        self.value_label.setText('Frames: {}'.format(self.ctrl.device_ctrl.frames.size()))
        self.start_btn.setText('Start')
        self.start_btn.setDisabled(False)
        self.calibrate_btn.setDisabled(False)
        self.stop_btn.setDisabled(True)
        self._updateImage_()
        QMessageBox.information(self, 'Working Finish', 'Process colecting image probe is finished. ')
        self.ctrl.device_ctrl.get_frames().clear()

    def _updateImage_(self):
        params = self.parmsWdg.getParams()
        self.img = self.ctrl.image_arr(x=params.get('rows', 16), y=params.get('cols', 16))
        if self.askSaveCheck.isChecked():
            self.selectPathBtnClick()

        self.saveImage(self.img)

        self.gaml_img = QImage('img.png')
        self.gaml_pixmap = QPixmap(self.gaml_img)
        self.gaml_lbl.setPixmap(self.gaml_pixmap.scaled(self.gaml_lbl.size(), Qt.KeepAspectRatio))

    def _init_ctrl_(self):
        self.ctrl = GammalabCtrl(self.ports_box.port)
        self.stop_btn.clicked.connect(self.ctrl.stop)
        self.ctrl.probeFinish.connect(self._probe_finish_)
        self.ctrl.device_ctrl.devParamsChanged.connect(self.updateDevParmas)
        self.ctrl.device_ctrl.devCalibrate.connect(lambda: self.calibrate_btn.setDisabled(False))
        self.ctrl.device_ctrl.devCalibrate.connect(lambda: self.start_btn.setDisabled(False))

    def updateFramesCounter(self):
        self.value_label.setText('Frames: {}'.format(self.ctrl.device_ctrl.frames.size()))

    def changeMode(self, btn):
        self.mode = btn.text()
        self.parmsWdg.changeMode(mode=self.mode)

    def updateDevParmas(self):
        self.parmsWdg.updateDeviceParams(**self.ctrl.device_ctrl.dev_params)

    def updatePathLabel(self):
        self.saveLabel.setText('Image file directory: ' + self.save_path)

    def selectPathBtnClick(self):
        path = QFileDialog.getSaveFileName(self, 'Select image save path', 'All Files (*)')
        self.save_path = path[0]
        self.updatePathLabel()

    def askCheckChange(self):
        if self.askSaveCheck.isChecked():
            self.selectPathBtn.setDisabled(True)
        else:
            self.selectPathBtn.setDisabled(False)

    def saveImage(self, img):
        img = Image.fromarray(img, 'L')
        img.save('img.png')
        if self.askSaveCheck.isChecked():
            self.selectPathBtnClick()
            # img.save(self.save_path)
        else:
            if self.save_path == '':
                self.save_path = 'img.png'
        img.save(self.save_path)

    def extraSave(self):
        self.selectPathBtnClick()
        img = Image.fromarray(self.img, 'L')
        if self.save_path == '':
            self.save_path = 'img_t.png'
        img.save(self.save_path)

    def loadPatientBtnClick(self):
        path = QFileDialog.getOpenFileName(self, 'Select patient image', 'All Files (*)')
        self.patientImgPath = path[0]
        if self.patientImgPath == '':
            self.patientImgPath = 'gammalab/resources/blank.png'
        imgArr = arrFromImg(self.patientImgPath)
        binImg = (imgArr > 30).astype(np.uint8)
        binDisplayImg = np.where(binImg == 1, 200, 0).astype(np.uint8)
        img = Image.fromarray(binDisplayImg, 'L')
        img.save('patientDisplay.png')
        self.updatePatientImage()

    def updatePatientImage(self):
        self.patient_image = QImage('patientDisplay.png')
        self.patient_pixmap = QPixmap(self.patient_image)
        self.patient_lbl.setPixmap(self.patient_pixmap.scaled(self.patient_lbl.size(), Qt.KeepAspectRatio))

    def patientLinFromImg(self) -> str:
        imgArr = arrFromImg(self.patientImgPath)
        binImg = (imgArr > 30).astype(np.uint8)
        data = binImg.flatten()
        data = np.array2string(data, separator='', max_line_width=200)

        return data[1: -1]
