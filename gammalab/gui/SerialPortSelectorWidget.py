from PySide2.QtWidgets import QWidget, QComboBox

from gammalab.ctrl.SerialMonitor.SerialPortSelector import SerialPortSelector
from gammalab.gui.ui_elements.PortsCombo import PortsCombo


class SerialPortSelectorWidget(QWidget, SerialPortSelector):

    def __init__(self):
        super().__init__()

    def init_ui(self):
        self.combo_box = PortsCombo()

    @property
    def port(self) -> str:
        return super().port

    def ports(self) -> [str]:
        return super().ports()

    @staticmethod
    def available_ports() -> [str]:
        return super().available_ports()


