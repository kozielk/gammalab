from PySide2.QtCore import QMargins
from PySide2.QtWidgets import QWidget, QVBoxLayout

from gammalab.gui.GammalabWidget import GammalabWidget
from gammalab.gui.SerialMonitorWidget import SerialMonitorWidget


class MainWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.parent = parent
        self.setContentsMargins(QMargins(0, 0, 0, 0))
        self.init_ui()

    def init_ui(self):
        self.parent.statusBar().showMessage("UI ready", 3000)
        self.serial_monitor = SerialMonitorWidget()
        self.gammalab_wdg = GammalabWidget()
        self.serial_monitor.setVisible(False)

        layout = QVBoxLayout(self)
        layout.addWidget(self.gammalab_wdg)
        layout.addWidget(self.serial_monitor)
