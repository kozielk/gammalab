from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QMenuBar, QAction, QApplication

from gammalab.gui.MainWidget import MainWidget


class MenuBar(QMenuBar):
    def __init__(self, mainWidget: MainWidget):
        QMenuBar.__init__(self)
        self.main_wdg = mainWidget

        fileMenu = self.addMenu('&File')
        fileMenu.addAction(self.loadPatient())
        fileMenu.addAction(self.saveImg())
        fileMenu.addAction(self.exit())

        viewMenu = self.addMenu('Vie&w')
        viewMenu.addAction(self.showSerial())

    def exit(self):
        act = QAction(QIcon('exit.png'), '&Exit', self)
        act.setShortcut('Ctrl+Q')
        act.setStatusTip('Exit application')
        act.triggered.connect(QApplication.instance().quit)

        return act

    def showSerial(self):
        act = QAction(QIcon('exit.png'), 'Vie&w', self)
        act.setStatusTip('Show/ hide serial monitor')
        act.triggered.connect(lambda: self.main_wdg.serial_monitor.setVisible(not self.main_wdg.serial_monitor.isVisible()))

        return act


    def saveImg(self):
        act = QAction(QIcon('exit.png'), 'Save..', self)
        act.setStatusTip('Show/ hide serial monitor')
        act.triggered.connect(lambda: self.main_wdg.gammalab_wdg.extraSave())

        return act

    def loadPatient(self):
        act = QAction(QIcon('exit.png'), 'Load patient...', self)
        act.setStatusTip('Load patient image')
        act.triggered.connect(lambda: self.main_wdg.gammalab_wdg.loadPatientBtnClick())

        return act




