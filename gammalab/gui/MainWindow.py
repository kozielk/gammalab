from PySide2 import QtGui, QtCore
from PySide2.QtCore import Qt, QMargins
from PySide2.QtWidgets import QMainWindow
from gammalab.ctrl.Utils import get_style
from gammalab.gui.MainWidget import MainWidget
from gammalab.gui.MenuBar import MenuBar


# TODO
#   make icon and set


class App(QMainWindow):
    style_path = 'gammalab/resources/style.css'

    def __init__(self):
        super().__init__()
        # self.left = 40
        # self.top = 80
        # self.width = 1200
        # self.height = 800
        self.title = 'GammaLab'

        self.initUI()

    def initUI(self):
        # self.setGeometry(self.left, self.top, self.width, self.height)
        self.setStyleSheet(get_style(self.style_path))
        self.setContentsMargins(QMargins(0, 0, 0, 0))
        self.setWindowTitle(self.title)
        self.statusBar().showMessage('Loading UI items')
        # self.setWindowIcon(QtGui.QIcon('img/DPC.png'))

        main_widget = MainWidget(self)
        menu_bar = MenuBar(main_widget)
        self.setMenuBar(menu_bar)
        self.setCentralWidget(main_widget)
        self.show()

    # def closeEvent(self, event):
    #     # do stuff
    #     if can_exit:
    #         event.accept() # let the window close
    #     else:
    #         event.ignore()
