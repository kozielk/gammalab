from PySide2.QtWidgets import QWidget, QPushButton, QHBoxLayout


class GamlModeBtnWdg(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.freeBtn = QPushButton('Free')
        self.freeBtn.setCheckable(True)
        self.numberBtn = QPushButton('Number')
        self.numberBtn.setCheckable(True)
        self.timeBtn = QPushButton('Time')
        self.timeBtn.setCheckable(True)
        self.btn = [self.freeBtn, self.numberBtn, self.timeBtn]

        layout = QHBoxLayout(self)
        layout.addWidget(self.freeBtn)
        layout.addWidget(self.numberBtn)
        layout.addWidget(self.timeBtn)

    # def mode(self) -> str:

    # def btnClick(self):


