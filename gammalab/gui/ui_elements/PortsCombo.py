from PySide2.QtCore import Signal
from PySide2.QtGui import QMouseEvent
from PySide2.QtWidgets import QComboBox, QStyle, QStyleOptionComboBox


class PortsCombo(QComboBox):
    arrowClicked = Signal()

    def mousePressEvent(self, e: QMouseEvent):
        super().mousePressEvent(e)

        opt = QStyleOptionComboBox()
        opt.initFrom(self)
        opt.subControls = QStyle.SC_All
        opt.activeSubControls = QStyle.SC_None
        opt.editable = self.isEditable()
        cc = self.style().hitTestComplexControl(
            QStyle.CC_ComboBox, opt, e.pos(), self)
        if cc == QStyle.SC_ComboBoxArrow:
            self.arrowClicked.emit()
