import numpy as np
from PySide2.QtCore import Qt, QRect, QSize
from PySide2.QtGui import QPainter, QBrush, QColor
from PySide2.QtWidgets import QWidget, QSizePolicy


class GamlImageWdg(QWidget):
    data: np.array

    def __init__(self, data=np.zeros([8, 8], np.int), parent=None):
        super().__init__(parent)
        self._data = data
        self.setSizePolicy(
            QSizePolicy.MinimumExpanding,
            QSizePolicy.MinimumExpanding
        )

    def sizeHint(self):
        return QSize(320, 320)

    def paintEvent(self, event):
        painter = QPainter(self)
        brush = QBrush()
        brush.setStyle(Qt.SolidPattern)
        it = np.nditer(self._data, flags=['multi_index'], op_flags=['writeonly'])
        for pixel in it:
            color = pixel
            brush.setColor(QColor(int(color), int(color), int(color)))
            rect = QRect(it.multi_index[1] * 10, it.multi_index[0] * 10, 10, 10)
            painter.fillRect(rect, brush)

    def update_data(self, data):
        self._data = data
        # self.mapOnColor()
        self.update()

    def mapOnColor(self):
        maximum = np.max(self._data)
        val = maximum/255
        self._data = self._data/val

    @property
    def data(self):
        return self._data
