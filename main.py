import sys

from PySide2.QtWidgets import QApplication
from gammalab.gui.MainWindow import App

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

